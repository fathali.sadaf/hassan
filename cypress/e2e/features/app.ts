import { Given, Then } from '@badeball/cypress-cucumber-preprocessor';

Given('I visit the home page', () => {
  cy.visit('/');
});

Then('I click in the text input', () => {
  cy.get('input').click();
});

Then('I type apple into the search input', () => {
  cy.get('input').type('apple');
});

Then('I should see {int} items', (val: number) => {
  cy.get('ul').children().should('have.length', val);
});

Given('I remove input text', () => {
  cy.get('input').clear();
});

Then('I should see an empty list', () => {
  cy.get('ul').children().should('have.length', 0);
});

Given('I type less than {int} character into search input', (chars: number) => {
  cy.get('input').type('a'.repeat(chars));
});

Then('I should see an empty list in result', () => {
  cy.get('ul').children().should('have.length', 0);
});
