Feature: Hassan website test scenarios
  Scenario: visiting the home page - successful search
    Given I visit the home page
    Then I click in the text input
    Then I type apple into the search input
    Then I should see 5 items

    Given I remove input text
    Then I should see an empty list

    Given I type less than 2 character into search input
    Then I should see an empty list in result