// import logo from './logo.svg';
import './App.css';
import Autocomplete from './Autocomplete';

function App() {
  return (
    <div className='container p-10 mx-auto'>
      <Autocomplete />
    </div>
  );
}

export default App;
