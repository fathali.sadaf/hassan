import Item from './Item';
import { useState } from 'react';

function Autocomplete() {
  const [search, setSearch] = useState('');

  const items = [
    { id: 1, title: 'Blueberry Delight' },
    { id: 2, title: 'Apple Bliss' },
    { id: 3, title: 'Mango Tango' },
    { id: 4, title: 'Orange Burst' },
    { id: 5, title: 'Pineapple Dream' },
    { id: 6, title: 'Lemon Zest' },
    { id: 7, title: 'Strawberry Spark' },
    { id: 8, title: 'Raspberry Ripple' },
    { id: 9, title: 'Banana Split' },
    { id: 10, title: 'Cherry Chomp' },
    { id: 11, title: 'Grape Pop' },
    { id: 12, title: 'Watermelon Splash' },
    { id: 13, title: 'Kiwi Crush' },
    { id: 14, title: 'Passionfruit Punch' },
    { id: 15, title: 'Guava Glaze' },
    { id: 16, title: 'Apricot Aura' },
    { id: 17, title: 'Blackberry Blast' },
    { id: 18, title: 'Cranberry Craze' },
    { id: 19, title: 'Pomegranate Paradise' },
    { id: 20, title: 'Mixed Berry Magic' },
  ];
  // const [list, setList] = useState(items);

  const filterdList =
    search && search.length > 2
      ? items.filter((item) =>
          item.title.toLowerCase().includes(search.toLowerCase())
        )
      : [];

  return (
    <div className=''>
      <div className='inline-flex flex-col justify-center relative text-gray-500'>
        <div className='relative'>
          <input
            type='text'
            className='p-2 pl-8 rounded border border-gray-200 bg-gray-200 focus:bg-white focus:outline-none focus:ring-2 focus:ring-yellow-600 focus:border-transparent'
            placeholder='search...'
            value={search}
            onChange={(e) => {
              setSearch(e.target.value);
            }}
          />
          <svg
            className='w-4 h-4 absolute left-2.5 top-3.5'
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'>
            <path
              stroke-linecap='round'
              stroke-linejoin='round'
              stroke-width='2'
              d='M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z'
            />
          </svg>
        </div>
        <ul className='bg-white border border-gray-100 w-full mt-2'>
          {filterdList.map((item) => (
            <Item key={item.id} title={item.title} />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Autocomplete;
